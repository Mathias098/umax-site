<footer data-aos="fade-up">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-2">
                <img class="img-fluid" src="{{asset('/images/logo.png')}}" alt="Logo UMAX">
            </div>
            <div class="col-md-5">
                <div class="pl-5">
                    <h6>Lun - Vie 08.00 - 21.00; Sab 08.00 - 12.00</h6>
                    <h6>+595 21296900</h6>
                    <h6 class="mb-3">info@umax.edu.py</h6>
                    <small>Copyright © 2020 - UMAX. &nbsp Power by <a href="http://www.nano.com.py/" target="_blanck">@nanodeveloper</a></small>
                </div>
            </div>
            <div class="col-md-4">
                <h6 class="m-3">Seguinos</h6>
                <div class="d-flex">
                    <a href="https://www.facebook.com/UMAXPAR/" target="_blank"><h4 class="mx-3"><ion-icon name="logo-facebook"></ion-icon></h4></a>
                    <a href="https://www.linkedin.com/company/37796802/admin/" target="_blank"><h4 class="mx-3"><ion-icon name="logo-linkedin"></ion-icon></h4></a>
                    <a href="https://twitter.com/UMAXPY" target="_blank"><h4 class="mx-3"><ion-icon name="logo-twitter"></ion-icon></h4></a>
                    <a href="https://www.instagram.com/umaxparaguay/" target="_blank"><h4 class="mx-3"><ion-icon name="logo-instagram"></ion-icon></h4></a>
                    <a href="https://www.youtube.com/channel/UCet3SULPyR8rw178xoY5v0g" target="_blank"><h4 class="mx-3"><ion-icon name="logo-youtube"></ion-icon></h4></a>
                </div>
            </div>
            <div class="col-md-1">
                <a href='#' class="scroll-top">
                    <ion-icon name="chevron-up-outline"></ion-icon>
                </a>
            </div>
        </div>
    </div>
</footer>