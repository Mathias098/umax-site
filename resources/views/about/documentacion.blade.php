@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Convenios</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

   {{--  <section class="university">
        <div class="requisitos">
            <div class="container">
                <h3 class="mb-5" data-aos="fade-up">Requisitos de inscripción</h3>
                <div class="row">
                    <div class="col-md-6" data-aos="fade-right" data-aos-duration="800">
                        <h4 class="text-primary">Nacionales</h4>
                        <p>Copia de Cédula de Identidad</p> 
                        <p>Copia de Certificado de Estudio</p>
                        <small>OBS. Los alumnos tendrán un tiempo límite de 60 días paran presentar todos sus documentos originales (Radicación o Escolar y Certificado de Nacimiento o de Matrimonio) a partir del día de inicio de clases.</small>
                        <h4 class="mt-5">Matriculación</h4>
                        <p>Certificado de Estudios de la Educación Media original.<br>
                            Certificado de Nacimiento original.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Estudios de la Educación Media.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Nacimiento.<br>
                            2 fotocopias autenticadas por Escribanía de la cédula de identidad.<br>
                            3 fotos tipo carnet con fondo blanco o rojo (medidas 3x4 cm).</p>

                        <h4 class="mt-5">Convalidación de Asignaturas</h4>
                        <p class="mb-0"><b>Proveniente de Universidades paraguayas</b></p>
                        <p>Certificado de Estudios de la carrera de grado original con Legalización del MEC.<br>
                            Certificado de Programas de Estudios original con Legalización del MEC.<br>
                            Declaración jurada de autenticidad de los documentos entregados (proveído por UMAX).</p>
                        <a href="#" class="btn btn-primary mt-4 mb-5">Descargar documentos</a>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <h4 class="text-primary">Extranjeros</h4>
                        <p>Copia de Cédula de Identidad</p> 
                        <p>Copia de Certificado de Estudio</p>
                        <small>OBS. Los alumnos tendrán un tiempo límite de 60 días paran presentar todos sus documentos originales (Radicación o Escolar y Certificado de Nacimiento o de Matrimonio) a partir del día de inicio de clases.</small>
                        <h4 class="mt-5">Matriculación</h4>
                        <p>Certificado de Estudios de la Educación Media, fotocopia con Apostilla original.<br>
                            Certificado de Nacimiento, fotocopia con Apostilla original.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Estudios de la Educación Media.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Nacimiento.<br>
                            2 fotocopias autenticadas por Escribanía del documento de identidad (RG, DNI, Pasaporte).<br>
                            2 fotocopias autenticadas por Escribanía del carnet de Radicación Permanente o Temporaria fotos tipo carnet con fondo blanco o rojo (medidas 3cm X 4cm).</p>

                        <h4 class="mt-5">Convalidación de Asignaturas</h4>
                        <p class="mb-0"><b>Proveniente de Universidades extranjeras</b></p>
                        <p>Certificado de Estudios de la carrera de grado original con Apostilla.<br>
                            Certificado de Programas de Estudios original con Apostilla.<br>
                            Declaración jurada de autenticidad de los documentos entregados (proveído por UMAX).</p>
                        <a href="#" class="btn btn-primary mt-4 mb-5">Descargar documentos</a>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.form-inscripcion')

    </section> --}}

    @section('scripts')
       {{--  <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 3.5,
                dots: '#scrollLockDelay',
                draggable: true,
                arrows: {
                    next: '.glider-next',
                    prev: '.glider-prev'
                }
            });
        </script> --}}
    @endsection

@endsection