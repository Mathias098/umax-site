<section class="form-carrera">
    <div class="form">
        <h3 class="mb-5" data-aos="fade-right"
        data-aos-duration="500">Elige tu futuro</h3>
        <form>
            <div class="form-group" data-aos="fade-up">
                <select class="form-control custom-select" id="selectCarrera">
                    <option>Carrera</option>
                    <option>Medicina</option>
                    <option>Enfermeria</option>
                </select>
            </div>
            <div class="form-group" data-aos="fade-up">
                <input type="text" class="form-control" id="inputName" placeholder="Nombre y Apellido">
            </div>
            <div class="form-group" data-aos="fade-up">
                <input type="email" class="form-control" id="inputEmail" placeholder="Corrreo electrónico">
            </div>
            <div class="form-group mb-5" data-aos="fade-up">
                <input type="text" class="form-control" id="inputCelular" placeholder="Celular">
            </div>
            <a href="#" class="btn btn-primary" data-aos="fade-up">Quiero Inscribirme</a>
        </form>
    </div>
    <div class="banner">
        <img src="{{ asset('images/estudiante.png') }}" class="img-fluid" alt="Imagen de Estudiante"
        data-aos="fade-left"
        data-aos-duration="500">
    </div>
</section>