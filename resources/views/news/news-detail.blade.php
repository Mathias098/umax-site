@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top mb-0">
        <div class="container">
            <h2 data-aos="fade-up">A la opinión pública</h2>
        </div>
        <img src="{{ asset('images/news.png') }}" alt="" class="w-100">
    </div>
    <section class="news mt-0 bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card" data-aos="fade-left">
                        <div class="">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones referentes a estudiantes brasileños</h3>
                        </div>
                        <div class="card-img-top">
                            <img src="{{ asset('images/news.png') }}" class="img-fluid" alt="...">
                        </div>
                    </div>
                    <p class="mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore delectus illo saepe vel quidem expedita, dolor tempore eius atque molestiae, sint nesciunt eveniet consequatur sit aut iure ullam. Consequuntur iure veritatis debitis, non sit commodi hic cumque soluta id dicta voluptatem veniam laborum illo voluptatibus nostrum quae obcaecati provident quam eveniet possimus blanditiis enim minima necessitatibus. Id quis porro dolorum maxime consequuntur officiis deleniti, voluptas tenetur saepe? Amet quod, enim ipsa doloremque laudantium nesciunt vitae sapiente voluptates rem sint fuga id distinctio debitis, quo quibusdam rerum officiis aperiam porro. Quo eveniet eaque impedit rerum quia iure consectetur maxime temporibus eos corrupti delectus, provident earum exercitationem cum ipsam dolor? Iure ab quo iste ratione libero veniam rerum porro laborum aspernatur! Repudiandae itaque consequatur id vel? Sit voluptatum minima quisquam nulla debitis. Laboriosam doloremque fugiat quae magni, accusantium ab veniam non eveniet soluta adipisci veritatis obcaecati ipsum libero omnis iste, neque velit.</p>

                    <div class="share mt-5">
                        <div class="d-flex">
                            <h6>Compartir</h6>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-facebook"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-linkedin"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-twitter"></ion-icon></h4></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection