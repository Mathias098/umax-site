@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Información y Requisitos</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">PostgradosHabilitación de Carreras</h3>
                <p data-aos="fade-up" data-aos-duration="800">La Universidad Maria Auxiliadora ofrece programas de formación continua al grado, a través de programas de posgrado de calidad y excelencia académica en los diferentes campos del saber. En la actualidad, los graduados pueden acceder a programas de maestrías, especializaciones y diplomados.</p>
                
                <h4 class="mt-5" data-aos="fade-up">Requisitos de admisión</h4>
                <p data-aos="fade-up" data-aos-duration="800">(1) Fotocopia Autenticada por Escribanía del Título de Grado registrado y legalizado por el MEC. Y registrado y visado por el rectorado (Si el candidato es egresado de la UNA). 
                <br>   
                (1) Fotocopia Autenticada por Escribanía del Certificado de Estudios de Grado. 
                <br>
                (1) Fotocopia Autenticadas por Escribanía del Documento de Identidad. 
                <br>
                (2) Fotos Tipo Carnet (3cm x 4cm).</p>

                <h4 class="mt-5" data-aos="fade-up">Para Extranjeros:</h4>
                <p data-aos="fade-up" data-aos-duration="800">(2) Fotocopias Autenticadas por Escribanía del Pasaporte o Documento de Identidad expedido por el país de origen. 
                    <br>
                    (2) Fotocopias Autenticadas por Escribanía del Carnet de Radicación expedido por la Dirección General de Migraciones.
                </p>
            </div>
        </div>
    </section>

@endsection