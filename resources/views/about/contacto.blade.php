@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Contacto</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <div class="contact">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-6" data-aos="fade-right">
                    <h3 class="mb-4">Dejanos un mensaje</h3>
                    <form>
                        <div class="form-group mb-4" data-aos="fade-up">
                            <input type="text" class="form-control" id="inputName" placeholder="Nombre y Apellido">
                        </div>
                        <div class="form-group" data-aos="fade-up">
                            <input type="email" class="form-control" id="inputEmail" placeholder="Corrreo electrónico">
                        </div>
                        <div class="form-group" data-aos="fade-up">
                            <input type="text" class="form-control" id="inputPhone" placeholder="Celular">
                        </div>
                        <div class="form-group mb-4" data-aos="fade-up">
                            <textarea class="form-control" id="mensaje" placeholder="Mensaje" rows="3"></textarea>
                        </div>
                        <div class="d-flex justify-content-md-end mt-5" data-aos="fade-up">
                            <a href="#" class="btn btn-primary">Enviar mensaje</a>
                        </div>
                    </form>
                </div>
                <div class="col-md-5 offset-1">
                    <h3 class="mb-4" data-aos="fade-left">Información de contacto</h3>
                    <h4 data-aos="fade-up">Mario Halley Mora c/ Palo Santo. Mariano Roque Alonso - Paraguay.</h4>
                    <h4 data-aos="fade-up">Lun - Vie 08.00 - 21.00; Sab 08.00 - 12.00</h4>
                    <a href="tel:+59521296900"><h4 class="my-5" data-aos="fade-up">+595 21296900</h4></a>
                    <a href="mailto:info@umax.edu.py"><h4 data-aos="fade-up">info@umax.edu.py</h4></a>
                </div>
            </div>
        </div>
        <div class="mt-5 py-5">
            <div class="container">
                <h4 class="mt-5" data-aos="fade-up">Visitanos</h4>
                <div class="mapa" data-aos="fade-up">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d115458.11603389579!2d-57.6626722!3d-25.2683609!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da5cc419fbb4f%3A0x99bf9e1fa69b6033!2sUniversidad%20Maria%20Auxiliadora%20(SEDE%20NUEVA)!5e0!3m2!1ses!2spy!4v1599128101352!5m2!1ses!2spy" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>

@endsection