@extends('layout.app')

@section('head')

@endsection

@section('content')

    @include('home.slider')

    <section class="about mb-5">
        <div class="container">
            <div class="row align-items-center mb-3">
                <div class="col-md-5">
                    <h5 data-aos="fade-up">Sobre nosotros</h5>
                    <h2 data-aos="fade-up">La Universidad <br>María Auxiliadora</h2>
                </div>
                <div class="col-md-7">
                    <p class="mb-0" data-aos="fade-up">La Universidad María Auxiliadora fue creada por Ley de la Nación Nº 3.501/08 de conformidad a lo que establece el Artículo 79 de la Constitución Nacional que textualmente expresa: “Las universidades son autónomas. Las universidades, tanto públicas como privadas, serán creadas por ley”. <a href="">Continuar leyendo</a></p>
                </div>
            </div>
        </div>
        <!-- Glider-->
        <div class="glider-contain my-5">
            <div class="glider">
                <div class="ml-0" data-aos="fade-left">
                    <img src="{{ asset('images/s1.jpg') }}" class="d-block w-100" alt="IMG">
                </div>
                <div class="ml-1" data-aos="fade-left">
                    <img src="{{ asset('images/s2.jpg') }}" class="d-block w-100" alt="IMG">
                </div>
                <div class="ml-1" data-aos="fade-left">
                    <img src="{{ asset('images/s3.jpg') }}" class="d-block w-100" alt="IMG">
                </div>
                <div class="ml-1" data-aos="fade-left">
                    <img src="{{ asset('images/s4.jpg') }}" class="d-block w-100" alt="IMG">
                </div>
                <div class="ml-1" data-aos="fade-left">
                    <img src="{{ asset('images/s5.jpg') }}" class="d-block w-100" alt="IMG">
                </div>{{-- 
                <div class="ml-1">
                    <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                </div>
                <div class="ml-1">
                    <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                </div> --}}
            </div>
            <div class="d-flex justify-content-center">
                <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i></button>
                <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i></button>
            </div>
        </div>
        <!-- End Glider-->
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-4" data-aos="fade-up">Por qué elegirnos</h3>
                    <div class="about-date d-flex">
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0"><ion-icon name="ribbon-outline"></ion-icon></h3>
                            <h4 class="ml-2 mb-0">Educación Superior Certificada</h4>
                        </div>
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0"><ion-icon name="star-outline"></ion-icon></h3>
                            <h4 class="ml-2 mb-0">27 años formando líderes</h4>
                        </div>
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0"><ion-icon name="people-circle-outline"></ion-icon></h3>
                            <h4 class="ml-2 mb-0">Más de 1200 Egresados</h4>
                        </div>
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0"><ion-icon name="business-outline"></ion-icon></h3>
                            <h4 class="ml-2 mb-0">Instalaciones modernas y de primer nivel</h4>
                        </div>
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0"><ion-icon name="earth-outline"></ion-icon></h3>
                            <h4 class="ml-2 mb-0">Convenios Nacionales e Internacionales</h4>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

    @include('home.slider-testimonios')

    @include('partials.form-inscripcion')

    <section class="news">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7" data-aos="fade-up">
                    <h5>Noticias</h5>
                    <h2>Conoce las noticias destacadas de la universidad</h2>
                </div>
                <div class="col-md-5" data-aos="fade-up">
                    <a href="{{ route('news') }}" class="btn btn-primary float-right">Ver más noticias</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card" data-aos="fade-right">
                        <div class="card-img-top">
                            <img src="{{ asset('images/news.png') }}" class="img-fluid" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones referentes a estudiantes brasileños</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card" data-aos="fade-left">
                        <div class="card-img-top">
                            <img src="{{ asset('images/news.png') }}" class="img-fluid" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones referentes a estudiantes brasileños</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @section('scripts')
        <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 3.5,
                dots: '#scrollLockDelay',
                draggable: true,
                arrows: {
                    next: '.glider-next',
                    prev: '.glider-prev'
                }
            });
        </script>
    @endsection

@endsection

