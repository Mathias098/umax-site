@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Virtual</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="pr-md-3">
                        <h3 data-aos="fade-up">Estudiar virtual</h3>
                        <div class="d-flex mt-4">
                            <div class="d-flex">
                                <h4 class="text-primary mr-2"><ion-icon name="add-circle-outline"></ion-icon></h4>
                                <h6 class="mb-0">Facilita la formación</h6>
                            </div>
                            <div class="d-flex ml-4">
                                <h4 class="text-primary mr-2"><ion-icon name="add-circle-outline"></ion-icon></h4>
                                <h6 class="mb-0">Permite Flexibilidad horaria</h6>
                            </div>
                            <div class="d-flex ml-4">
                                <h4 class="text-primary mr-2"><ion-icon name="add-circle-outline"></ion-icon></h4>
                                <h6 class="mb-0">Aporta Comodidad</h6>
                            </div>
                        </div>        
                        <p class="mt-2">
                            La Universidad, en el marco de la mejora continua, ha tomado importantes decisiones con la finalidad de garantizar el desarrollo de las actividades académicas, en tal sentido y en tiempo récord ha logrado:
                            <ul>
                                <li class="text-muted">Implementar una plataforma utilizada a nivel mundial.</li>
                                <li class="text-muted">Capacitado a los docentes en el uso de las herramientas tecnológicas.</li>
                                <li class="text-muted">Adecuación y mejora en infraestructura tecnológica.</li>
                            </ul>
                        </p>
                        <p class="mt-2">
                            Dentro de las Ventajas de la plataforma Moodle - UMAX, podemos resaltar los siguientes puntos:
                            <ul>
                                <li class="text-muted">Facilita la comunicación de los docentes y estudiantes fuera del horario de clases.</li>
                                <li class="text-muted">Ayuda al aprendizaje cooperativo ya que permite la comunicación a distancia mediante foros, correo y chat.</li>
                                <li class="text-muted">Adaptable a las necesidades de la Institución.</li>
                                <li class="text-muted">Se encuentra traducido a más de 70 idiomas.</li>
                                <li class="text-muted">Permite la utilización de distintos tipos de recursos y formatos, para que el docente entregue a sus estudiantes.</li>
                                <li class="text-muted">Permite llevar un registro de acceso de los estudiantes y un historial de las actividades de cada estudiante y docente </li>
                            </ul>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3 class="mb-4" data-aos="fade-up">Acceder a la plataforma</h3>
                    <form>
                        <div class="form-group" data-aos="fade-up">
                            <input type="email" class="form-control" id="inputEmail" placeholder="Correo electrónico">
                        </div>
                        <div class="form-group mb-4" data-aos="fade-up">
                            <input type="password" class="form-control" id="inputPass" placeholder="Contraseña">
                        </div>
                        <a href="#" class="text-dark" data-aos="fade-up">¿Olvidó su nombre de usuario o contraseña?</a>
                        <div class="mt-5">
                            <a href="#" class="btn btn-primary btn-block" data-aos="fade-up">Iniciar Sesión</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection