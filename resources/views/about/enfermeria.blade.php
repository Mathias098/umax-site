@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Enfermería</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="habilitacion" data-aos="fade-up">
                <div class="row align-items-center">
                    <div class="col-md-3">
                        <h3>Habilitación y acreditación de la carrera</h3>
                    </div>
                    <div class="col-md-3">
                        <div class="d-flex">
                            <img src="{{ asset('images/aneaes.png') }}" alt="" class="w-50">
                            <img src="{{ asset('images/cones.png') }}" alt="" class="w-50">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <p>Reconocimiento de habilitación de la carrera de Medicina por el Consejo Nacional de Educación Superior (CONES)</p>
                    </div>
                    <div class="col-md-3">
                        <a href="#" class="btn btn-primary">Ver Reconocimiento</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="mensaje">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="pr-5">
                            <h5 data-aos="fade-up">MENSAJE DE LA DECANA DE LA</h5>
                            <h3 data-aos="fade-up" data-aos-duration="200">Facultad de Ciencias de la Salud</h3>
                            <p data-aos="fade-up" data-aos-duration="800">Para quienes estamos al frente de la Facultad de Ciencias de la Salud, seguros estamos
                            de  apostar por una educación superior de calidad,  realizando el trabajo diario con
                            mucho ahínco, entusiasmo y energía, orientándonos  siempre en el firme compromiso
                            de generar la excelencia que redunde en la formación profesional de los estudiantes
                            basándonos en los tres pilares fundamentales de la Educación Superior.
                            <br><br>    
                            Transmitimos el orgullo y la satisfacción de haber logrado la Acreditación de la Carrera
                            de Enfermería por la Agencia Nacional de Evaluación y Acreditación de la Educación
                            Superior (ANEAES), cerrando el año 2017. Todos somos el corazón no solamente por la
                            función que desarrollamos siempre en la línea de la formación en salud, sino porque
                            somos también el sentimiento, el cariño, la empatía para con nuestros estudiantes, al
                            estar siempre acompañando su proceso de  formación, para garantizarles de los
                            conocimientos técnicos, prácticos  y profesionales.
                            <br><br>
                            Aportamos en esta tarea educativa  valores como la  responsabilidad, el respeto  por la
                            dignidad y la humanización de la formación para un cuidado de calidad y calidez, pues
                            de esa forma, estamos contribuyendo también a consolidar el  prestigio de nuestra
                            querida Casa de Estudios.
                            <br><br>
                            El agradecimiento y reconocimiento a  los Docentes, quienes día a día sienten
                            satisfacción del trabajo bien hecho que  se puede ver reflejado en los egresados de las
                            distintas promociones, quienes hoy día están incorporados en el campo laboral,
                            desempeñándose  en cargos de importancia.
                            <br><br>
                            No podemos dejar de mencionar nuestros agradecimientos a los estudiantes con
                            quienes compartimos la noble tarea de formarlos en las aulas  y reiterarles que el
                            compromiso es seguir llevando en alto  siempre el nombre de nuestra Universidad
                            María Auxiliadora, haciendo de la práctica diaria una costumbre de superación en el
                            ámbito personal, profesional  y  humano.
                            <br><br>
                            La Facultad de Ciencias de la Salud agradece a la Comunidad Educativa los deseos de
                            superación profesional y expresa una cordial bienvenida a las carreras que preparan al
                            estudiante para incorporarse como miembro del  Equipo de Salud, que con
                            conocimiento científico, capacidades, destrezas y actitudes cimentadas en valores,
                            permitirán aportar a las personas, estilos de vida saludables, protección de la salud,
                            recuperación y rehabilitación de la misma.
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="avatar">
                            <img src="{{ asset('images/dra-lilian.png') }}" alt="" class="img-fluid" data-aos="fade-left" data-aos-duration="500">
                            <p class="pr-5 mt-4" data-aos="fade-up" data-aos-duration="800">Prof. M.SC. María Lilian Portelli</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="text-nosotros">             
                <h4 class="mt-5" data-aos="fade-up">Misión</h4>
                <p data-aos="fade-up" data-aos-duration="800">Formar Licenciados en Enfermería competentes, creativos, innovadores, autónomos y responsables para
                    el cuidado integral de la salud mediante un modelo educativo con enfoque holístico que promueva la
                    calidez y la calidad, que contribuya con el crecimiento y desarrollo biopsicosocial de las personas, de la
                    comunidad local, regional y global.</p>

                <h4 class="mt-5" data-aos="fade-up">Visión</h4>
                <p data-aos="fade-up" data-aos-duration="800">Formar Licenciados en Enfermería competentes, creativos, innovadores, autónomos y responsables para
                    el cuidado integral de la salud mediante un modelo educativo con enfoque holístico que promueva la
                    calidez y la calidad, que contribuya con el crecimiento y desarrollo biopsicosocial de las personas, de la
                    comunidad local, regional y global.</p>

                <h4 class="mt-5" data-aos="fade-up">Perfil del Egresado</h4>
                <p data-aos="fade-up" data-aos-duration="800">La carrera de Licenciatura en Enfermería, propone que el egresado sea un profesional competente, con
                    una visión vanguardista, que demuestre conocimientos científicos, técnicos y humanísticos,
                    desempeñándose con eficacia, responsabilidad y sentido humanitario en forma individual como en
                    equipos multidisciplinarios, así como ejecutar un estilo de liderazgo para la toma de decisiones en el
                    campo de la Enfermería proporcionando la modificación de hábitos y estilos de vida para la
                    conservación de la salud y el autocuidado, generar en el bienestar del individuo y su familia, fomentar
                    conductas para el autocuidado de la salud y del entorno en las diversas etapas de la vida del individuo.
                </p>

            </div>
        </div>
        <div class="malla">
            <div class="container">
                <h3 class="mb-5" data-aos="fade-left">Malla Curricular</h3>
                <div class="malla-tab" data-aos="fade-up">
                    <nav class="nav nav-pills flex-column flex-sm-row">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer" role="tab" aria-controls="primer" aria-selected="true">PRIMER AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo" role="tab" aria-controls="segundo" aria-selected="false">SEGUNDO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero" role="tab" aria-controls="tercero" aria-selected="false">TERCERO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto" role="tab" aria-controls="cuarto" aria-selected="false">CUARTO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto" role="tab" aria-controls="quinto" aria-selected="false">QUINTO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#sexto" role="tab" aria-controls="sexto" aria-selected="false">SEXTO AÑO</a>
                    </nav>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Lengua Castellana y Comunicación</p> 
                                    <p>Anatomía y Fisiología Humana</p>
                                    <p>Matemática</p>
                                    <p>Fundamentos y Tecnologías en  Enfermería I</p>
                                    <p>Introducción a la Enfermería</p>
                                    <p>Primeros Auxilios</p>
                                    <p>Psicología General</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Farmacología I</p> 
                                    <p>Metodología de la Investigación Científica</p>
                                    <p>Microbiología y Parasitología</p>
                                    <p>Fundamentos y Tecnologías de  Enfermería II</p>
                                    <p>Biología</p>
                                    <p>BEnfermería en Salud Pública</p>
                                    <p>Laboratorio(Practica Institucional)</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="segundo" role="tabpanel" aria-labelledby="segundo-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tercero" role="tabpanel" aria-labelledby="tercero-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="cuarto" role="tabpanel" aria-labelledby="cuarto-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-center mt-5 justify-content-md-between">
                    <div data-aos="fade-right">
                        <h4>Título a obtener: LICENCIADO/A EN ENFERMERIA</h4>
                        <h4>TOTAL HORAS: 4.344 horas reloj</h4>
                        <h4>EXTENSION UNIVERSITARIA: 40 horas reloj</h4>
                    </div>
                    <a href="{{ asset('records/brochure-enfermeria.pdf') }}" class="btn btn-primary" data-aos="fade-left" target="blank">DESCARGAR BROCHURE</a>
                </div>
            </div>
        </div>
        <div class="requisitos">
            <div class="container">
                <h3 class="mb-4" data-aos="fade-up">Requisitos de inscripción</h3>
                <div class="row">
                    <div class="col-md-6" data-aos="fade-right" data-aos-duration="800">
                        <h4 class="text-primary">Nacionales</h4>
                        <p>Copia de Cédula de Identidad</p> 
                        <p>Copia de Certificado de Estudio</p>
                        <small>OBS. Los alumnos tendrán un tiempo límite de 60 días paran presentar todos sus documentos originales (Radicación o Escolar y Certificado de Nacimiento o de Matrimonio) a partir del día de inicio de clases.</small>
                        <h4 class="mt-5">Matriculación</h4>
                        <p>Certificado de Estudios de la Educación Media original.<br>
                            Certificado de Nacimiento original.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Estudios de la Educación Media.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Nacimiento.<br>
                            2 fotocopias autenticadas por Escribanía de la cédula de identidad.<br>
                            3 fotos tipo carnet con fondo blanco o rojo (medidas 3x4 cm).</p>

                        <h4 class="mt-5">Convalidación de Asignaturas</h4>
                        <p class="mb-0"><b>Proveniente de Universidades paraguayas</b></p>
                        <p>Certificado de Estudios de la carrera de grado original con Legalización del MEC.<br>
                            Certificado de Programas de Estudios original con Legalización del MEC.<br>
                            Declaración jurada de autenticidad de los documentos entregados (proveído por UMAX).</p>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <h4 class="text-primary">Extranjeros</h4>
                        <p>Copia de Cédula de Identidad</p> 
                        <p>Copia de Certificado de Estudio</p>
                        <small>OBS. Los alumnos tendrán un tiempo límite de 60 días paran presentar todos sus documentos originales (Radicación o Escolar y Certificado de Nacimiento o de Matrimonio) a partir del día de inicio de clases.</small>
                        <h4 class="mt-5">Matriculación</h4>
                        <p>Certificado de Estudios de la Educación Media, fotocopia con Apostilla original.<br>
                            Certificado de Nacimiento, fotocopia con Apostilla original.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Estudios de la Educación Media.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Nacimiento.<br>
                            2 fotocopias autenticadas por Escribanía del documento de identidad (RG, DNI, Pasaporte).<br>
                            2 fotocopias autenticadas por Escribanía del carnet de Radicación Permanente o Temporaria fotos tipo carnet con fondo blanco o rojo (medidas 3cm X 4cm).</p>

                        <h4 class="mt-5">Convalidación de Asignaturas</h4>
                        <p class="mb-0"><b>Proveniente de Universidades extranjeras</b></p>
                        <p>Certificado de Estudios de la carrera de grado original con Apostilla.<br>
                            Certificado de Programas de Estudios original con Apostilla.<br>
                            Declaración jurada de autenticidad de los documentos entregados (proveído por UMAX).</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="autoridades">
            <div class="container">
                <h5 data-aos="fade-up">Nomina de</h5>
                <h3 class="mb-5" data-aos="fade-up">Autoridades de Enfermería</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-lilian-2.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Prof. M.Sc. María Lilian Portelli.</h4>
                                <h6>Decana de la Facultad de Ciencias de la Salud.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-loren.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>M. Sc. Ana Loren Vega Quiñónez.</h4>
                                <h6>Directora Académica.</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.form-inscripcion')

    </section>

    @section('scripts')
        {{--  <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 3.5,
                dots: '#scrollLockDelay',
                draggable: true,
                arrows: {
                    next: '.glider-next',
                    prev: '.glider-prev'
                }
            });
        </script> --}}
    @endsection

@endsection