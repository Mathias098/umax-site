<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
})->name('home');

Route::get('/nosotros', function () {
    return view('about.index');
})->name('about');

Route::get('/medicina', function () {
    return view('about.medicina');
})->name('medicina');

Route::get('/enfermeria', function () {
    return view('about.enfermeria');
})->name('enfermeria');

Route::get('/virtual', function () {
    return view('about.virtual');
})->name('virtual');

Route::get('/documentacion', function () {
    return view('about.documentacion');
})->name('documentacion');

Route::get('/contacto', function () {
    return view('about.contacto');
})->name('contacto');

Route::get('/postgrado', function () {
    return view('postgrado.index');
})->name('postgrado');

Route::get('/maestria', function () {
    return view('postgrado.maestria');
})->name('maestria');

Route::get('/especializacion', function () {
    return view('postgrado.especializacion');
})->name('especializacion');

Route::get('/diplomado', function () {
    return view('postgrado.diplomado');
})->name('diplomado');

Route::get('/extension', function () {
    return view('extension.index');
})->name('extension');

Route::get('/investigacion', function () {
    return view('extension.investigacion');
})->name('investigacion');

Route::get('/pastoral', function () {
    return view('extension.pastoral');
})->name('pastoral');

Route::get('/noticias', function () {
    return view('news.index');
})->name('news');

Route::get('/noticias/1', function () {
    return view('news.news-detail');
})->name('news-detail');