@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Investigación</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Importancia de la Investigación</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    La Universidad María Auxiliadora entiende que la investigación es trascendental en las actividades académicas. Pues la importancia de la investigación radica en la capacidad que tienen las ciencias de contribuir en la construcción de la sociedad y en la dignificación humana por medio de su labor investigativa y de sus constantes descubrimientos. La investigación, por tanto, tiene su lugar privilegiado dentro de la comunidad educativa de la Universidad María Auxiliadora, pues por medio de esta comunidad educativa busca constantemente el descubrimiento de algo nuevo en el ámbito científico y, por ende, la construcción de una sociedad pensante, crítica y proactiva.
                    <br><br>
                    <b>La dirección de Investigación está a cargo del Prof. MS.c. Arsenio Ramón Amaral Rodríguez, Lic. En Filosofía, Universidad Católica de Asunción; 2. Periodista; 3. Máster interuniversitario en Profesorado de Calidad para la docencia Preuniversitaria y Universitaria por la Universidad Ca’Fóscari de Venecia. 4. Doctorando en Filosofía por la Universidad Pontificia de Salamanca (Reino de España)</b>
                </p>
                <h4 class="mt-5" data-aos="fade-up">Misión</h4>
                <p data-aos="fade-up" data-aos-duration="800">“Generar, promover, difundir y preservar el desarrollo de investigaciones de calidad, formando docentes y estudiantes investigadores y generando proyectos que respondan a los avances de la ciencia y tecnología y en respuesta a las necesidades sociales”.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Visión</h4>
                <p data-aos="fade-up" data-aos-duration="800">“Ser una dirección líder en investigación, que promueva enfoques integrales que puedan incidir en la construcción de una sociedad nacional e internacional eficiente, justa y  sustentable basada en valores éticos”.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Objetivos</h4>
                <p data-aos="fade-up" data-aos-duration="800">Fomentar y fortalecer el área con el desarrollo de investigaciones de calidad que generen conocimientos que impacten en la formación integral de los estudiantes, egresados, docentes e investigadores involucrados y retribuyan a la sociedad el aporte dado.
                </p>
            </div>
        </div>
    </section>

@endsection