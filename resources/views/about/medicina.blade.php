@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Medicina</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="habilitacion" data-aos="fade-up">
                <div class="row align-items-center">
                    <div class="col-md-3">
                        <h3>Habilitación y acreditación de la carrera</h3>
                    </div>
                    <div class="col-md-3">
                        <div class="d-flex">
                            <img src="{{ asset('images/aneaes.png') }}" alt="" class="w-50">
                            <img src="{{ asset('images/cones.png') }}" alt="" class="w-50">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <p>Reconocimiento de habilitación de la carrera de Medicina por el Consejo Nacional de Educación Superior (CONES)</p>
                    </div>
                    <div class="col-md-3">
                        <a href="#" class="btn btn-primary">Ver Reconocimiento</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="mensaje">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="pr-5">
                            <h5 data-aos="fade-up">MENSAJE DEL DECANO DE LA</h5>
                            <h3 data-aos="fade-up" data-aos-duration="200">Carrera de Medicina</h3>
                            <p data-aos="fade-up" data-aos-duration="800">La Medicina nos elige. Nosotros obedecemos a un llamado que nos empuja a realizarnos a través de todas las dificultades, obstáculos, esfuerzo y cansancio que implica seguir a esa voz interior
                            <br><br>    
                            La recompensa es grande, por supuesto, pero no es algo tan visible como las cosas materiales que señalan el éxito para la Sociedad. Es algo intenso y discreto que se da cuando somos conscientes que hemos ayudado a la Naturaleza a paliar el sufrimiento de alguien o cuando percibimos que somos un instrumento divino al participar del milagro de un nacimiento o al acompañar a alguien que se está yendo, sabiendo que hemos hecho todo lo posible por él.
                            <br><br>
                            Estudiar Medicina tiene un comienzo, pero no tiene un final. Es algo imposible de acabar, porque como un ser vivo, crece, cambia y siempre tiene sorpresas y giros que nos obligan a estar pendiente de él. Es una pasión que nos consume, porque queremos saber siempre más y no nos conformamos nunca.
                            <br><br>
                            Es para personas especiales, que sienten el sufrimiento de los demás como propio y buscan a través del conocimiento del Arte de Curar, una herramienta para aliviar al prójimo. Como dijo Samuel Taylor Coleridge, <i><b>“El mejor médico es el que mejor inspira la esperanza”.</b></i>
                            <br><br>
                            Si eso es lo que te mueve a estudiar Medicina, adelante! Y que se cumpla contigo lo dicho por Hipócrates: <i><b>“Algunos pacientes, aunque conscientes de que su condición es peligrosa, recuperan su salud simplemente por su satisfacción con la bondad del médico.”</b></i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="avatar">
                            <img src="{{ asset('images/dr-gustavo.png') }}" alt="" class="img-fluid" data-aos="fade-left" data-aos-duration="500">
                            <p class="pr-5 mt-4" data-aos="fade-up" data-aos-duration="800">Dr. Gustavo Guillermo Calvo</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="text-nosotros">             
                <h4 class="mt-5" data-aos="fade-up">Misión</h4>
                <p data-aos="fade-up" data-aos-duration="800">La carrera de Medicina tiene como misión formar profesionales con un alto nivel científico, competitivo, con pensamiento crítico y reflexivo, con capacidad de integrarse al equipo interdisciplinario de salud, con sólidos principios éticos y compromiso con las personas, familia, comunidad, y un elevado nivel de inteligencia emocional que le permita adecuarse equilibradamente a su realidad local, nacional e internacional, en el ámbito de su competencia y en los distintos niveles de atención al sistema de salud, así como en el campo de la investigación, docencia y extensión social.</p>

                <h4 class="mt-5" data-aos="fade-up">Visión</h4>
                <p data-aos="fade-up" data-aos-duration="800">Ser una carrera con excelencia académica y alto sentido de responsabilidad social, líder en la formación de profesionales Médicos, capaces de confrontar, modificar y plantear soluciones a los problemas de salud de su entorno, donde la docencia, la investigación y la extensión hagan parte integral del proceso formativo del estudiante, generando conocimientos que conlleven al mejoramiento de la calidad de vida de la sociedad paraguaya.</p>

                <h4 class="mt-5" data-aos="fade-up">Perfil del Egresado</h4>
                <p data-aos="fade-up" data-aos-duration="800">El Médico de la Universidad María Auxiliadora será un profesional de reconocida excelencia, que se desempeñe con responsabilidad y en forma competente en la red de salud de las personas y de la población, reconociendo los derechos de los pacientes, de la confidencialidad del secreto profesional y del consentimiento informado.
                <br><br>
                El egresado podrá participar en los ámbitos de la promoción, prevención, recuperación y rehabilitación a lo largo del ciclo vital, incorporando una visión humanista con respecto a la familia, la comunidad y su entorno sociocultural.
                <br><br>
                El Médico tendrá razonamiento crítico y reflexivo acerca de su rol social, así como de su labor y profesión, la que ejercerá sobre la base del conocimiento actualizado, considerando el avance de las ciencias, la tecnología y los cambios de los determinantes de la salud, incorporando aquellas innovaciones relevantes para la práctica clínica.
                <br><br>
                Será capaz de trabajar efectivamente en los diferentes equipos de salud, en escenarios diversos en cuanto a complejidad y contexto, en coherencia con los principios y valores éticos, espirituales y morales.
                <br><br>
                El egresado desarrollará una relación de comunicación empática, utilizando los idiomas oficiales, generando acciones orientadas a resolver las necesidades y expectativas de salud del país, optimizando los procesos de gestión desde una perspectiva estratégico-operativa, con el fin de mejorar la salud de la población.
                <br><br>
                Se destacará por su liderazgo y positivismo en sus ámbitos de desempeño, consciente de su propia necesidad de crecimiento y formación profesional, realizando actividades de perfeccionamiento continuo y así contribuir a la generación de conocimiento, proyectándose como un referente de excelencia, ya sea como Médico General o Especialista, Administrador o Académico y/o Investigador en el ámbito de la salud.</p>

            </div>
        </div>
        <div class="malla">
            <div class="container">
                <h3 class="mb-5" data-aos="fade-left">Malla Curricular</h3>
                <div class="malla-tab" data-aos="fade-up">
                    <nav class="nav nav-pills flex-column flex-sm-row">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer" role="tab" aria-controls="primer" aria-selected="true">PRIMER AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo" role="tab" aria-controls="segundo" aria-selected="false">SEGUNDO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero" role="tab" aria-controls="tercero" aria-selected="false">TERCERO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto" role="tab" aria-controls="cuarto" aria-selected="false">CUARTO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto" role="tab" aria-controls="quinto" aria-selected="false">QUINTO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#sexto" role="tab" aria-controls="sexto" aria-selected="false">SEXTO AÑO</a>
                    </nav>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="segundo" role="tabpanel" aria-labelledby="segundo-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tercero" role="tabpanel" aria-labelledby="tercero-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="cuarto" role="tabpanel" aria-labelledby="cuarto-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="quinto" role="tabpanel" aria-labelledby="quinto-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="sexto" role="tabpanel" aria-labelledby="sexto-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p> 
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-center mt-5 justify-content-md-between">
                    <div data-aos="fade-right">
                        <h4>Título a obtener: MEDICO</h4>
                        <h4>TOTAL HORAS: 8.340 horas reloj</h4>
                        <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                    </div>
                    <a href="{{ asset('records/brochure-medicina.pdf') }}" class="btn btn-primary" data-aos="fade-left" target="_blank ">DESCARGAR BROCHURE</a>
                </div>
            </div>
        </div>
        <div class="requisitos">
            <div class="container">
                <h3 class="mb-4" data-aos="fade-up">Requisitos de inscripción</h3>
                <div class="row">
                    <div class="col-md-6" data-aos="fade-right" data-aos-duration="800">
                        <h4 class="text-primary">Nacionales</h4>
                        <p>Copia de Cédula de Identidad</p> 
                        <p>Copia de Certificado de Estudio</p>
                        <small>OBS. Los alumnos tendrán un tiempo límite de 60 días paran presentar todos sus documentos originales (Radicación o Escolar y Certificado de Nacimiento o de Matrimonio) a partir del día de inicio de clases.</small>
                        <h4 class="mt-5">Matriculación</h4>
                        <p>Certificado de Estudios de la Educación Media original.<br>
                            Certificado de Nacimiento original.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Estudios de la Educación Media.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Nacimiento.<br>
                            2 fotocopias autenticadas por Escribanía de la cédula de identidad.<br>
                            3 fotos tipo carnet con fondo blanco o rojo (medidas 3x4 cm).</p>

                        <h4 class="mt-5">Convalidación de Asignaturas</h4>
                        <p class="mb-0"><b>Proveniente de Universidades paraguayas</b></p>
                        <p>Certificado de Estudios de la carrera de grado original con Legalización del MEC.<br>
                            Certificado de Programas de Estudios original con Legalización del MEC.<br>
                            Declaración jurada de autenticidad de los documentos entregados (proveído por UMAX).</p>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <h4 class="text-primary">Extranjeros</h4>
                        <p>Copia de Cédula de Identidad</p> 
                        <p>Copia de Certificado de Estudio</p>
                        <small>OBS. Los alumnos tendrán un tiempo límite de 60 días paran presentar todos sus documentos originales (Radicación o Escolar y Certificado de Nacimiento o de Matrimonio) a partir del día de inicio de clases.</small>
                        <h4 class="mt-5">Matriculación</h4>
                        <p>Certificado de Estudios de la Educación Media, fotocopia con Apostilla original.<br>
                            Certificado de Nacimiento, fotocopia con Apostilla original.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Estudios de la Educación Media.<br>
                            1 fotocopia autenticada por Escribanía del Certificado de Nacimiento.<br>
                            2 fotocopias autenticadas por Escribanía del documento de identidad (RG, DNI, Pasaporte).<br>
                            2 fotocopias autenticadas por Escribanía del carnet de Radicación Permanente o Temporaria fotos tipo carnet con fondo blanco o rojo (medidas 3cm X 4cm).</p>

                        <h4 class="mt-5">Convalidación de Asignaturas</h4>
                        <p class="mb-0"><b>Proveniente de Universidades extranjeras</b></p>
                        <p>Certificado de Estudios de la carrera de grado original con Apostilla.<br>
                            Certificado de Programas de Estudios original con Apostilla.<br>
                            Declaración jurada de autenticidad de los documentos entregados (proveído por UMAX).</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="campos">
            <div class="container">
                <h3 class="mb-4">Campos de prácticas</h3>
                <div class="row">
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title mb-0">Hospital Materno Infantil de Limpio.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title mb-0">Hospital Materno Infantil Loma Pyta.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title mb-0">Instituto Nacional de Cardiología Prof. Dr. Juan Adolfo Cattoni – Hospital San Jorge.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title mb-0">Hospital Regional de Caacupé.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title mb-0">Hospital Materno Infantil Santísima Trinidad.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title mb-0">Hospital Distrital Mariano Roque Alonso.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="autoridades">
            <div class="container">
                <h5 data-aos="fade-up">Nomina de</h5>
                <h3 class="mb-5" data-aos="fade-up">Autoridades de Medicina</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-gustavo-2.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Gustavo Guillermo Calvo.</h4>
                                <h6>Decano de la Facultad de Medicina.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/fachada3.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Iván Javier Díaz Rolón.</h4>
                                <h6>Coordinador de Ciencias Básicas.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dra-jessica.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dra. Jessica Nilse Candia Lezcano.</h4>
                                <h6>Coordinadora de Pre – Clínicas.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-cristian.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Cristian Pestana Sierra.</h4>
                                <h6>Coordinador de Clínicas e Internado.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/rector.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Luis Ovelar Ferreira.</h4>
                                <h6>Asistente de Coordinación de Clínicas e Internado.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/fachada3.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>M. Sc. Elvira Samaniego.</h4>
                                <h6>Coordinadora Pedagógica.</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.form-inscripcion')

    </section>

    @section('scripts')
        {{--  <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 3.5,
                dots: '#scrollLockDelay',
                draggable: true,
                arrows: {
                    next: '.glider-next',
                    prev: '.glider-prev'
                }
            });
        </script> --}}
    @endsection

@endsection