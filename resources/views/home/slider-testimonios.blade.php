<div class="testimonios my-5">
    <div class="container">
        <div id="carouselTestimonios" class="carousel slide" data-ride="carousel">
            <h3 data-aos="fade-right">Testimonios <br>de nuestros alumnos</h3>
            <a class="carousel-control-prev" href="#carouselTestimonios" role="button" data-slide="prev" data-aos="fade-left">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
            <a class="carousel-control-next" href="#carouselTestimonios" role="button" data-slide="next" data-aos="fade-left">
                <ion-icon name="chevron-forward-outline"></ion-icon>
            </a>
            <ol class="carousel-indicators">
                <li data-target="#carouselTestimonios" data-slide-to="0" class="active"></li>
                <li data-target="#carouselTestimonios" data-slide-to="1"></li>
                <li data-target="#carouselTestimonios" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" data-aos="fade-up">
                <div class="carousel-item active">
                    <div class="profile">
                        <div class="d-flex align-items-center">
                            <div class="picture">
                                <img src="{{asset('/images/perfil.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="date">
                                <h4 class="mb-0"><b>Juan Díaz</b></h4>
                                <small>Egresado de Medicina</small>
                            </div>
                        </div>
                        <div class="caption">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem</p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="profile">
                        <div class="d-flex align-items-center">
                            <div class="picture">
                                <img src="{{asset('/images/perfil.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="date">
                                <h4 class="mb-0"><b>Juan Díaz</b></h4>
                                <small>Egresado de Medicina</small>
                            </div>
                        </div>
                        <div class="caption">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem</p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="profile">
                        <div class="d-flex align-items-center">
                            <div class="picture">
                                <img src="{{asset('/images/perfil.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="date">
                                <h4 class="mb-0"><b>Juan Díaz</b></h4>
                                <small>Egresado de Medicina</small>
                            </div>
                        </div>
                        <div class="caption">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem</p>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>