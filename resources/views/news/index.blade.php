@extends('layout.app')

@section('head')

@endsection

@section('content')
    <div class="header-top mb-0">
        <div class="container">
            <h2 data-aos="fade-up">Noticias</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="news mt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card" data-aos="fade-left">
                        <div class="card-img-top">
                            <img src="{{ asset('images/news.png') }}" class="img-fluid" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones referentes a estudiantes brasileños</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-left">
                        <div class="card-img-top">
                            <img src="{{ asset('images/news.png') }}" class="img-fluid" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones referentes a estudiantes brasileños</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection