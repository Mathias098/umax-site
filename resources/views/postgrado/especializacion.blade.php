@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Especialización</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Especialización en <br> Didáctica Superior Universitaria</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    La educación en general no está ajena a las innovaciones y a los pensamientos contemporáneos, esto motiva a los profesionales del ámbito a actualizarse constantemente. Por ello, es importante crear espacios que permitan analizar e interactuar sobre los nuevos cambios en la educación, las tendencias en este campo. 
                    <br><br>
                    Un profesional, dedicado a la docencia universitaria que cuente con un postgrado orientado hacia la didáctica, posibilita que pueda desempeñarse con capacidad diferenciadora, además le agrega valor a su dedicación como docente y contribuye a calidad en su desempeño pedagógico y esto mismo reditúa en la calidad de la educación.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Objetivo General del Programa</h4>
                <p data-aos="fade-up" data-aos-duration="800">El programa tienen pretende formar y potenciar a los profesionales para el ejercicio de la docencia universitaria con conocimientos teóricos-prácticos para la enseñanza en la Educación Superior.
                <br><br>    
                Propicia espacios para la construcción de aprendizajes que posibiliten a los docentes universitarios el desarrollo de competencias específicas y transversales para planificar, ejecutar y evaluar procesos de enseñanza y aprendizaje, en el marco de los enfoques pedagógicos y curriculares emergentes, considerando fundamentalmente, los criterios de pertinencia y calidad.
                <br><br>
                Además de Incorporar y promover el uso adecuado de la Tecnología de la Información y la Comunicación (TIC) como herramienta fundamental para dinamizar y mejorar permanentemente los procesos educativos universitarios, a través de la formación de recursos humanos en el área de didáctica universitaria. 
                <br><br>
                Elaborar estrategias didácticas, métodos y técnicas de enseñanza-aprendizaje de su especialidad profesional, conforme a los criterios técnicos que aseguren su calidad y pertinencia. 
                <br><br>
                <b>Título que Otorga:</b> Especialista en Didáctica Superior Universitaria. 
                <br>
                <b>Duración del Programa:</b> 11 (once) meses, con un total de 362 horas reloj, organizado en módulos.</p>
            </div>
        </div>
        <div class="malla">
            <div class="container">
                {{-- <h3 class="mb-5">Módulos que Componen el Programa</h3> --}}
                <div class="malla-tab">
                    <nav class="nav nav-pills flex-column flex-sm-row">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer" role="tab" aria-controls="primer" aria-selected="true" data-aos="fade-up">Módulos que Componen el Programa</a>
                    </nav>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                            <div class="row" data-aos="fade-up" data-aos-duration="800">
                                <div class="col-md-6">
                                    <p>Tendencias y Políticas Educativas en la Educación Superior</p> 
                                    <p>Pedagogía en la Educación Superior</p> 
                                    <p>Modelos Educativos y Teoría del Aprendizaje</p> 
                                    <p>Didáctica en la Educación Superior</p> 
                                    <p>Currículo en la Educación Superior</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Planeamiento y Proceso en Enseñanza Aprendizaje</p> 
                                    <p>Evaluación de los Aprendizajes</p> 
                                    <p>Tecnología Aplicada a la Educación Superior</p> 
                                    <p>Metodología de la Investigación Científica</p> 
                                    <p>Redacción del Informe de Investigación</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="d-flex align-items-center mt-5 justify-content-md-between">
                    <div>
                        <h4>Título a obtener: MEDICO</h4>
                        <h4>TOTAL HORAS: 8.340 horas reloj</h4>
                        <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                    </div>
                    <a href="#" class="btn btn-primary">DESCARGAR BROCHURE</a>
                </div> --}}
            </div>
        </div>
    </section>

@endsection