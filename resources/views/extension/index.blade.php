@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Extensión Universitaria</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Justificación y Objetivos</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    La labor de la Extensión Universitaria como la función que, articulada a la docencia e investigación, promueve la interacción permanente con su propia comunidad universitaria y con los demás actores de la sociedad. 
                    <br><br>
                    Aportando desde su quehacer con el conocimiento y desde los principios y valores que inspiran su labor educativa, propuestas y solución a las necesidades y problemas que enfrenta la sociedad.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Justificación</h4>
                <p data-aos="fade-up" data-aos-duration="800">La extensión universitaria, forma parte integral de su tarea educativa, y que es fundamento de su sentido y propósito. La institución procura que profesores, alumnos y demás miembros del claustro universitario se comprometan libremente, en unidad de vida, con coherencia de pensamiento, palabra y acción, a buscar, descubrir, comunicar y conservar la verdad, en todos los campos del conocimiento, con fundamento en una concepción cristiana del hombre y del mundo, como contribución al progreso de la sociedad.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Objetivos</h4>
                <p data-aos="fade-up" data-aos-duration="800">Promover la participación de los estudiantes universitarios a fin de inculcar la responsabilidad social. 
                    <br><br>
                    Fortalecer y promocionar las actividades universitarias en los sectores sociales y productivos con el fin de lograr un mayor impacto, así como también recibir información que retroalimente los proyectos y programas de vinculación con la finalidad de adecuarlos a las necesidades de los sectores sociales y de desarrollo universitario. 
                    <br><br>
                    Fomentar la formación de grupos multidisciplinarios para la ejecución del proyecto de extensión. Delinear objetivos y metas que produzcan impactos sociales destinados a satisfacer las necesidades de la comunidad y a mejorar sus condiciones de vida. 
                    <br><br>
                    Organizar e impulsar el desarrollo de las actividades culturales, artísticas, y deportivas, así como de obtener recursos para las actividades de extensión.
                </p>
            </div>
        </div>
        <div class="videos">
            <div class="container">
                <h3 class="mb-4" data-aos="fade-up">Videos</h3>
                <div class="row">
                    <div class="col-md-4">
                        <div class="image-prev" data-aos="fade-left">
                            <a href="#" class="btn btn-play"><ion-icon name="play-sharp"></ion-icon></a>
                            <img src="{{ asset('images/fachada3.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="image-prev" data-aos="fade-left">
                            <a href="#" class="btn btn-play"><ion-icon name="play-sharp"></ion-icon></a>
                            <img src="{{ asset('images/fachada3.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="image-prev" data-aos="fade-left">
                            <a href="#" class="btn btn-play"><ion-icon name="play-sharp"></ion-icon></a>
                            <img src="{{ asset('images/fachada3.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fotos pt-5">
            <div class="container">
                <h3 class="mt-5 py-5" data-aos="fade-up">Fotos</h3>
            </div>
            <!-- Glider-->
            <div class="glider-contain mb-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/s1.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s2.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s3.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s4.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s5.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>{{-- 
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div> --}}
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i></button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i></button>
                </div>
            </div>
            <!-- End Glider-->
        </div>
    </section>

    @section('scripts')
        <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 3.5,
                dots: '#scrollLockDelay',
                draggable: true,
                arrows: {
                    next: '.glider-next',
                    prev: '.glider-prev'
                }
            });
        </script>
    @endsection

@endsection