@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Sobre nosotros</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pr-0" data-aos="fade-right">
                    <img src="{{ asset('images/fachada2.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-md-6 px-0" data-aos="fade-left" style="background-color: #F6F6FA;">
                    <div class="text">
                        <h3 class="mb-4">La Universidad</h3>
                        <p>La Universidad María Auxiliadora fue creada por Ley de la Nación Nº 3.501/08 de conformidad a lo que establece el Artículo 79 de la Constitución Nacional que textualmente expresa: “Las universidades son autónomas. Las universidades, tanto públicas como privadas, serán creadas por ley”. 
                        <br><br>
                        El Proyecto de Creación fue aprobado por la Honorable Cámara de Senadores en fecha 20 de diciembre de 2.007 y sancionado por la Honorable Cámara de Diputados en fecha 05 de junio de 2.008. El Poder Ejecutivo ha promulgado la Ley de la Nación Nº 3.501/08 en fecha 23 de junio de 2.008.</p>
                    </div>
                </div>
            </div>

            <div class="text-nosotros">
                <h4 data-aos="fade-up">Habilitación de Carreras</h4>
                <p data-aos="fade-up" data-aos-duration="800">En fecha 16 de julio de 2.008 la Universidad María Auxiliadora ha habilitado la carrera Ingeniería en Ciencias de la Informática de conformidad a lo que establece el Artículo 10 de su Estatuto, el Artículo 79 de la Constitución Nacional y el Artículo 5 de la Ley Nº 2.529/06 “Que modifica parcialmente la Ley Nº 136/93 “DE UNIVERSIDADES”.
                <br><br>    
                En fecha 05 de setiembre de 2.008 la Universidad María Auxiliadora ha habilitado la carrera MEDICINA de conformidad a lo que establece el Artículo 10 de su Estatuto, el Artículo 79 de la Constitución Nacional y el Artículo 5 de la Ley Nº 2.529/06 “Que modifica parcialmente la Ley Nº 136/93 “DE UNIVERSIDADES”.
                <br><br>
                En fecha 17 de marzo de 2017 El Consejo de Educación Superior (CONES) por Resolución 133/17 ha habilitado la carrera MEDICINA de la Sede Central de Loma Pyta – Asunción de conformidad a la Resolución 166/15 e insertada en el Registro Nacional de Ofertas Académicas.</p>
                
                <h4 class="mt-5" data-aos="fade-up">Reconocimiento de Títulos</h4>
                <p data-aos="fade-up" data-aos-duration="800">Los títulos de Licenciatura en Enfermería a ser emitidos por la Universidad María Auxiliadora cuentan con el reconocimiento oficial del Ministerio de Educación y Ciencias según Resolución Nº 908/08 de fecha 07 de Agosto de 2.008 expedida por la Dirección General de Educación Superior.
                <br><br>   
                Los títulos de Medicina a ser emitidos por la Universidad María Auxiliadora cuentan con el reconocimiento oficial del Ministerio de Educación y Ciencias según Nota DGES Nº 786 de fecha 17 de Marzo de 2.009 expedida por la Dirección General de Educación Superior.</p>
                <div class="row my-5">
                    <div class="col-md-6">
                        <h4 class="mt-5" data-aos="fade-up">Misión</h4>
                        <p data-aos="fade-up" data-aos-duration="800">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est</p>
                    </div>
                    <div class="col-md-6">
                        <h4 class="mt-5" data-aos="fade-up">Visión</h4>
                        <p data-aos="fade-up" data-aos-duration="800">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="valores">
            <div class="container">
                <div class="row text-center justify-content-center">
                    <div class="col-md-10">
                        <h3 data-aos="fade-up">Principios y Valores</h3>
                        <p class="mb-5" data-aos="fade-up">Es por ello que la Institución se ha propuesto enfatizar su actividad
                            <br>en dos áreas del saber como la SALUD y la EDUCACIÓN.</p>
                        <h6 class="item" data-aos="flip-down">Integridad</h6>
                        <h6 class="item" data-aos="flip-down">Compromiso con la Calidad</h6>
                        <h6 class="item" data-aos="flip-down">Cordialidad</h6>
                        <h6 class="item" data-aos="flip-down" data-aos-duration="600">Espiritualidad Cristiana</h6>
                        <h6 class="item" data-aos="flip-down" data-aos-duration="600">Responsabilidad Social</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="mensaje">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="pr-5">
                            <h5 data-aos="fade-up">Mensaje del Rector</h5>
                            <h3 data-aos="fade-up" data-aos-duration="200">Estimados Alumnos:</h3>
                            <p data-aos="fade-up" data-aos-duration="800">El espíritu de nuestra casa de estudios nos motiva y nos empuja al crecimiento integral de la persona y hacemos de este nuestra razón de ser, formando recursos humanos con alta idoneidad.
                            <br><br>
                            Los desafíos actuales del mercado laboral y de la sociedad demandan un alto entrenamiento técnico con fino desarrollo interpersonal y combinando esto con una firme disciplina, generan una fórmula exacta y precisa para lograr el éxito.
                            <br><br>
                            Sabemos y asumimos que la salud es un área sensible y muy delicada de nuestro país. Partiendo de esto desarrollamos proyectos educativos minuciosos, serios, estrictos y con proyección encuadradas en las exigencias actuales.
                            <br><br>
                            Nuestra Institución educativa desde 1993 está formando excelentes profesionales en salud basados en la exigencia académica y el énfasis en los campos de práctica.
                            <br><br>
                            Formar a los “Mejores Profesionales en Salud” del Paraguay es posible gracias a que tenemos un objetivo bien definido, un grupo humano calificado, trayectoria, tradición e infraestructura a disposición de las personas que confían en nosotros.
                            <br><br>
                            Anímate a formar parte de la única Universidad enfocada exclusivamente en formar a los <b>“Mejores Egresados en Salud”.</b></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="avatar">
                            <img src="{{ asset('images/rector.png') }}" alt="" class="img-fluid" data-aos="fade-left" data-aos-duration="600">
                            <p class="pr-5 mt-4" data-aos="fade-up" data-aos-duration="800">Prof. Dr. Hernando Javier Quiñonez Sarabia.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="autoridades">
            <div class="container">
                <h2 class="mb-5" data-aos="fade-up">Autoridades</h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/rector-2.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Prof. Dr. Hernando Javier Quiñonez Sarabia.</h4>
                                <h6>Rector</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-elvira.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Prof. M. Sc. Elvira Peralta Ortigoza.</h4>
                                <h6>Presidenta del C.S.U.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-gloria.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Prof. M. Sc. Gloria Concepción Martinez Pasmor.</h4>
                                <h6>Vicerrectora Académica.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-steven.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>M. Sc. Rubén Steven Falcón Gamarra.</h4>
                                <h6>Secretario General.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-viviana.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Lic. Viviana Carina Pompa Arias.</h4>
                                <h6>Directora General Administrativa.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-gustavo.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Gustavo Guillermo Calvo.</h4>
                                <h6>Decano de la Facultad de Medicina.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-lilian-2.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Prof. M.Sc. María Lilian Portelli.</h4>
                                <h6>Decana de la Facultad de Ciencias de la Salud.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dra-fanny.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dra. Fanny Graciela Ayala Ratti.</h4>
                                <h6>Directora de Calidad Académica.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-ricardo.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Ricardo Elvis Garay.</h4>
                                <h6>Director de Extensión Universitaria.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-arsenio.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>M.Sc Arsenio Ramón Amaral Rodríguez.</h4>
                                <h6>Director de Investigación.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-nayila.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Lic. Nayila García.</h4>
                                <h6>Directora de Pastoral.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/ing-cinthia.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Ing. Cinthia Carolina Maiz Galeano.</h4>
                                <h6>Directora de Marketing.</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="insfra">
            <div class="container">
                <h2 class="mb-4" data-aos="fade-up">Infraestructura</h2>
            </div>
            <!-- Glider-->
            <div class="glider-contain my-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/s1.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s2.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s3.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s4.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s5.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>{{-- 
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div> --}}
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i></button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i></button>
                </div>
            </div>
            <!-- End Glider-->
        </div>
    </section>

    @section('scripts')
        <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 3.5,
                dots: '#scrollLockDelay',
                draggable: true,
                arrows: {
                    next: '.glider-next',
                    prev: '.glider-prev'
                }
            });
        </script>
    @endsection

@endsection