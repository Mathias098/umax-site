@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Pastoral</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Justificación y Objetivos</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    Nuestra Pastoral Universitaria, está abocada al acompañamiento de la vida y el caminar de todos los miembros de la comunidad educativa, promoviendo un encuentro personal y comprometido con Jesucristo y la Santísima Virgen María, en innumerables iniciativas misioneras como solidarias. 
                    <br><br>
                    Así, los tres Pilares de la Pastoral Institucional son: 
                    <br><br>
                    <b>1. La Evangelización.</b> 
                    <br><br>
                    <b>2. La devoción a la Virgen María Auxiliadora.</b> 
                    <br><br>
                    <b>3. La Caridad.</b>
                    <br><br>
                    La evangelización responde a la misión que todos tenemos: la de anunciar la alegría de la Buena Nueva del Reino de Dios. 
                    <br><br>
                    La devoción a la Virgen María, puesto que la universidad lleva el nombre de la Madre de Jesús, en su advocación a “María Auxiliadora”. 
                    <br><br>
                    Sin obras de Caridad, no podrá llevarse a cabo la evangelización. La Institución desde sus orígenes realiza innumerables obras de caridad, a través de proyectos de ayuda social. 
                    <br><br>
                    Desde el año 2016 hasta la actualidad brinda a hogares, fundaciones y comunidades vulnerables asistencia médica comunitaria en: Clínica Médica, Psiquiatría, Ginecología, Pediatría, Medicina familiar, como parte de la extensión universitaria. 
                    <br><br>
                    Continuaremos empeñados con la ayuda de Nuestro Dios y de la Santísima Virgen “María Auxiliadora”, en la realización de grandes proyectos de acción social y fe. 
                    <br><br>
                    ¡Te invitamos a formar parte de nuestra Pastoral Universitaria! 
                    <br><br>
                    ¡Dios y la Virgen los bendigan abundantemente!
                </p>
            </div>
        </div>
        <div class="fotos">
            <div class="container">
                <h3 class="mt-5 py-5" data-aos="fade-up">Fotos</h3>
            </div>
            <!-- Glider-->
            <div class="glider-contain mb-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/s1.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s2.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s3.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s4.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s5.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>{{-- 
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div> --}}
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i></button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i></button>
                </div>
            </div>
            <!-- End Glider-->
        </div>
    </section>

    @section('scripts')
        <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 3.5,
                dots: '#scrollLockDelay',
                draggable: true,
                arrows: {
                    next: '.glider-next',
                    prev: '.glider-prev'
                }
            });
        </script>
    @endsection

@endsection