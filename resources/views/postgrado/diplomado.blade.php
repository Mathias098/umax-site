@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Diplomado</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Diplomado en Metodología <br> de la Investigación Científica</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    El desarrollo y avance de las ciencias en el mundo son cada vez más rápidos por lo que constantemente las innovaciones y el desarrollo se imponen en la sociedad global. Y es menester, en este sentido, mencionar que para estos avances ha sido fundamental la presencia de la investigación en las áreas del saber que han generado investigación, desarrollo e innovación. Asimismo, es importante mencionar que estos avances han sido fundamentales y trascendentes en el desarrollo y progreso de las sociedades que los han desarrollado. Lo cual significa que la investigación, el desarrollo y la innovación son elementos esenciales para el desarrollo de los pueblos. 
                    <br><br>
                    La investigación contribuye a conocer y comprender los fenómenos sociales y económicos, a descubrir las causas de las inequidades, de la ineficiencia económica, de la discriminación de toda índole, y del atraso entre otras deficiencias del desarrollo.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Objetivo General del Programa</h4>
                <p data-aos="fade-up" data-aos-duration="800">Fortalecer las competencias referidas a: planificación investigativa, estrategia metodológica, sistematización de la información, análisis e interpretación de los datos, informe final de investigación y elaboración de artículos científicos.
                <br><br>
                <b>Título que Otorga:</b> Diplomado en  “Metodología de la Investigación Científica“
                <br>
                <b>Duración del Programa:</b> 6 (seis) meses, con un total de 98 horas reloj, organizado en
                módulos.</p>
            </div>
        </div>
        <div class="malla">
            <div class="container">
                {{-- <h3 class="mb-5">Módulos que Componen el Programa</h3> --}}
                <div class="malla-tab">
                    <nav class="nav nav-pills flex-column flex-sm-row">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer" role="tab" aria-controls="primer" aria-selected="true" data-aos="fade-up">Módulos que Componen el Programa</a>
                    </nav>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                            <div class="row" data-aos="fade-up" data-aos-duration="800">
                                <div class="col-md-6">
                                    <p>Epistemología de la Investigación Científica</p>
                                    <p>Proceso de la Investigación Científica</p>
                                    <p>Planteamiento del Problema de Investigación</p>
                                    <p>Fundamentación Teórica de la Investigación Científica</p>
                                    <p>Tipos de Investigación</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Diseños Metodológicos de la Investigación Científica</p>
                                    <p>El Proceso de Muestreo en la Investigación</p>
                                    <p>Instrumentos de Investigación</p>
                                    <p>Análisis de los Datos en la Investigación Científica</p>
                                    <p>Redacción de Informes de Investigación</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="d-flex align-items-center mt-5 justify-content-md-between">
                    <div>
                        <h4>Título a obtener: MEDICO</h4>
                        <h4>TOTAL HORAS: 8.340 horas reloj</h4>
                        <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                    </div>
                    <a href="#" class="btn btn-primary">DESCARGAR BROCHURE</a>
                </div> --}}
            </div>
        </div>
    </section>

@endsection