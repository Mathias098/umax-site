<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container px-0">
        <div class="logo" data-aos="fade-right">
            <img class="img-fluid" src="{{asset('/images/logo.png')}}" alt="Logo UMAX">
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon" data-aos="fade-left"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto align-items-center" data-aos="fade-left">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('home') }}">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">Nosotros</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdownMedicina" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Carreras
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMedicina">
                        <a class="dropdown-item" href="{{ route('medicina') }}">Medicina</a>
                        <a class="dropdown-item" href="{{ route('enfermeria') }}">Enfermería</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdownPostgrado" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Postgrados
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownPostgrado">
                        <a class="dropdown-item" href="{{ route('postgrado') }}">Información</a>
                        <a class="dropdown-item" href="{{ route('maestria') }}">Maestría</a>
                        <a class="dropdown-item" href="{{ route('especializacion') }}">Especialización</a>
                        <a class="dropdown-item" href="{{ route('diplomado') }}">Diplomado</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('virtual') }}">Virtual</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdownExtension" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Extensión
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownExtension">
                        <a class="dropdown-item" href="{{ route('investigacion') }}">Investigación</a>
                        <a class="dropdown-item" href="{{ route('extension') }}">Extensión Universitaia</a>
                        <a class="dropdown-item" href="{{ route('pastoral') }}">Pastoral</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('documentacion') }}">Convenios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Egresados</a>
                </li>
                <li class="nav-item mr-3">
                    <a class="nav-link" href="{{ route('contacto') }}">Contacto</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary my-auto" href="#" data-toggle="modal" data-target=".bd-example-modal-xl">Inscribirme</a>
                </li>
            </ul>
        </div>
    </div>
</nav>