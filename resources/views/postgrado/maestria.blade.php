@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Maestría</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Maestría en Educación <br> con énfasis en Educación Superior</h3>
                <h4 class="mt-5" data-aos="fade-up">Objetivo General del Programa</h4>
                <p data-aos="fade-up" data-aos-duration="800">El programa de Magister propicia la formación de profesionales en el ámbito de la docencia universitaria, que cuenten con los conocimientos necesarios de las teorías y principios filosóficos, epistemológicos, psicológicos y pedagógicos que fundamentan a la Educación Superior. Al tiempo de desarrollar la investigación educativa, aplicando enfoques y estrategias, tanto cuantitativas, como cualitativas. 
                <br><br>
                Sin perder de vista la capacidad de relacionar las políticas que caracterizaron el pasado cercano, el presente y las tendencias futuras del sistema de la educación superior. 
                <br><br>
                <b>Título que Otorga:</b> Magister en Educación con Énfasis en Educación Superior. 
                <br>
                <b>Duración del Programa:</b> 24 (veinticuatro) meses, con un total de 742 horas reloj, organizado módulos.</p>
            </div>
        </div>
        <div class="malla">
            <div class="container">
                {{-- <h3 class="mb-5">Módulos que Componen el Programa</h3> --}}
                <div class="malla-tab">
                    <nav class="nav nav-pills flex-column flex-sm-row">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer" role="tab" aria-controls="primer" aria-selected="true" data-aos="fade-up">Módulos que Componen el Programa</a>
                    </nav>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                            <div class="row" data-aos="fade-up" data-aos-duration="800">
                                <div class="col-md-6">
                                    <p>Tendencias y Políticas Educativas en la Educación Superior </p>
                                    
                                    <p>Pedagogía en la Educación Superior </p>

                                    <p>Modelos Educativos y Teoría del Aprendizaje</p> 

                                    <p> Didáctica en la Educación Superior</p> 

                                    <p>Currículo en la Educación Superior</p> 

                                    <p> Planeamiento y Proceso en Enseñanza Aprendizaje</p> 

                                    <p> Evaluación de los Aprendizajes</p> 

                                    <p> Tecnología Aplicada a la Educación Superior</p> 

                                    <p> Metodología de la Investigación Científica I</p>
                                    
                                    <p>Taller de Proyecto de Tesis I</p> 
                                    
                                </div>
                                <div class="col-md-6">
                                    <p>  Fundamentos Epistemológicos de las Ciencias de la Educación</p> 

                                    <p> Teorías Educativas Contemporáneas</p> 

                                    <p> Talleres de Elaboración: Guías – Plan de Clases – Recursos Informático.</p> 

                                    <p>  Formación Docente</p> 

                                    <p>  Metodología de la Investigación Científica II</p> 

                                    <p>  Diseño y Gestión de Proyectos Educativos</p>

                                    <p>  Diseño y Gestión de Proyectos Educativos</p>

                                    <p>   Evaluación y Análisis de Calidad en la Educación Superior</p> 

                                    <p>   Neuro Educación Taller de Proyecto de tesis II</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="d-flex align-items-center mt-5 justify-content-md-between">
                    <div>
                        <h4>Título a obtener: MEDICO</h4>
                        <h4>TOTAL HORAS: 8.340 horas reloj</h4>
                        <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                    </div>
                    <a href="#" class="btn btn-primary">DESCARGAR BROCHURE</a>
                </div> --}}
            </div>
        </div>
    </section>

@endsection